Read me flyfish exercise for POST pet API

I use cypess (javascript) to implement my solution.
Please find my solution in the below link:
https://gitlab.com/flyfish1/flyfish-exercise.git
(if you have any issue please send me an email mariose88@gmail.com)

I used 4 files to implement the project.

1. cypress\e2e\integration\pet.feature

Here you will find the scenarios I implemented using cucumber (BDD approach)
There are steps like validations are not the best solution because validations can be in the 1st step.
However I added them to increase the number of steps so you can see my logic.
In some scenarios I used examples to have reusable scenarios.

2. cypress\e2e\integration\steps\pet-steps.js

Here you can find the implementation steps.
Steps => functions

3. cypress\e2e\pet.js
In this file you can find the implementation of functions.
The logic is to run the apis ( i used POST and GET also fo validations)
Then I validate the fields from the body request and the response.
Also i checked some messages for negative scenarios

4. cypress\fixtures\data\pet.json

Here I have the "basic" sample body i use for my post request and i change the values i want in my functions



* I noticed that name and photo urls are mandatory fields in the POST API.
 However when I ran the specific scenarios I got 200 responses.
 In my solution i guess that was a bug and i implement my scenarios to validate an error message for mandatory fields.
 
 
Please do not hesitate to contact me for any other clarifications.
mariose88@gmail
Marios Efstathiou

