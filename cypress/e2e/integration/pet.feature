Feature: Pet Post Api testing

  Scenario Outline: Add pet succesfully with auto id and with spicific id
    Given Add pet with id "<id>" name "<name>" and photo url "<photoUrl>"
    Then Validate succesfully addition of pet
    Examples:
      | id | name  | photoUrl     |
      | 0  | Bruno | test exampe  |
      | 1  | Fox   | example test |


  Scenario: Add pet succesfully with multible photos
    Given Add pet with multible photos
    Then Validate succesfully addition of pet

  Scenario Outline: Add pet succesfully without mandatory fields
    Given Add pet without "field"
    Then Validate error for mandatory field

    Examples:
      | field    |
      | name     |
      | photoUrl |

  Scenario: Add pet succesfully without any details
    Given Add pet without info
    Then Validate error for mandatory field

  Scenario Outline: Try to add pet with ids as string
    Given Try to add pet with "<field>"
    Then Validate error for error input type "string"

    Examples:
      | field      |
      | petId      |
      | categoryId |
      | tagsId     |

  Scenario: Try to add pet with string fields as integers
    Given Try to add pet with "<field>"
    Then Validate error for error input type "integer"

    Examples:
      | field        |
      | name         |
      | photoUrl     |
      | categoryName |
      | tagName      |
      | status       |
