import { Given, Then, When } from 'cypress-cucumber-preprocessor/steps';
import { petpage } from '../../pet';



 Given("Add pet with id {string} name {string} and photo url {string}", (id,name,url) => {
    petpage.addPet(id,name,url)
  });

  Given("Add pet succesfully with multible photos", () => {
    petpage.addPetMultiblePhotos()
  });

  Given("Add pet without {string}", (field) => {
    petpage.addPetWithoutMandatory(field)
  });

  Given("Add pet without info", () => {
    petpage.addPetWithoutMandatory("all")
  });
  
  Given("Try to add pet with {string}", (field) => {
    petpage.addPetInvalid(field)
  });  

  Then("Validate succesfully addition of pet", () => {
    petpage.validateSuccesfulPetAddition();
  });

  Then("Validate error for mandatory field", () => {
    petpage.validateInvalidMandatory();
  });
  Then("Validate error for error input type {string}", (type) => {
    petpage.validateInvalidType();
  });


    