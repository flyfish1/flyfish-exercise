let fixture = 'data/pet';
var globalName;
var globalId;
var gloabalUrl;
var gloabalMultibleUrl;
var globalMessage;


export const petpage = {

  addPet(id, name, url) {
    cy.fixture(fixture).then((data) => {
      data.name = name;
      data.id = id;
      data.photoUrls[0] = url;
    });
    let body = data.petMandatory;
    cy.request({
      method: 'POST',
      url: "https://petstore.swagger.io/v2/pet",
      body: body,
      timeout: 60000
    }).then((resp) => {
      expect(resp.status).to.eq(200);
      expect(resp.body.id).not.to.be.null;
      expect(resp.body.name).not.to.be.null;
      globalName = resp.body.name;
      globvalId = resp.body.id;
      gloabalUrl = resp.body.photoUrls[0];
      gloabalMultibleUrl = false;
    });
  },

  addPetMultiblePhotos() {
    cy.fixture(fixture).then((data) => {
      data.photoUrls[0] = "url1";
      data.photoUrls[1] = "url2";
    });
    let body = data.petMandatory;
    cy.request({
      method: 'POST',
      url: "https://petstore.swagger.io/v2/pet",
      body: body,
      timeout: 60000
    }).then((resp) => {
      expect(resp.status).to.eq(200);
      expect(resp.body.id).not.to.be.null;
      expect(resp.body.name).not.to.be.null;
      globalName = resp.body.name;
      globvalId = resp.body.id;
      gloabalUrl = resp.body.photoUrls[0];
      gloabalMultibleUrl = true;
    });
  },

  validateSuccesfulPetAddition() {
    let path = "https://petstore.swagger.io/v2/pet/" + globalId;
    cy.fixture(fixture).then((data) => {
      cy.request({
        failOnStatusCode: false,
        method: 'GET',
        url: path
      }).then((resp) => {
        if (gloabalMultibleUrl.eq(true)) {
          expect("url1").to.eq(resp.body.photoUrls[0]);
          expect("url2").to.eq(resp.body.photoUrls[1]);
        }
        else {
          expect(gloabalUrl).to.eq(resp.body.photoUrls[0]);
        }
        expect(gloabalUrl).to.eq(resp.body.photoUrls[0]);
        expect(globalName).to.eq(resp.body.name);

        if (globvalId != 0)
          expect(globvalId).to.eq(resp.body.id);
        else
          expect(globalId) > 0;

        expect(resp.body.category.id).to.eq(data.category.id);
        expect(resp.body.category.name).to.eq(data.category.name);
        expect(resp.body.tags.id).to.eq(data.tags.id);
        expect(resp.body.tags.name).to.eq(data.tags.name);
        expect(resp.body.status).to.eq(data.status);
      });
    });
  },

  addPetWithoutMandatory(field) {
    cy.fixture(fixture).then((data) => {
      let body;
      switch (field) {
        case 'name':
          data.name = "";
          body = data.petMandatory;
          break;
        case 'photoUrl':
          data.photoUrls[0] = "";
          body = data.petMandatory;
          break;
        case 'all':
          body = "{}";
          break;
      }
    });
    cy.request({
      method: 'POST',
      url: "https://petstore.swagger.io/v2/pet",
      body: body,
      timeout: 60000
    }).then((resp) => {
      expect(resp.status).to.eq(400);
      globalMessage = resp.body.property('errorMessage');
      gloabalMultibleUrl = false;
    });
  },

  validateInvalidMandatory() {
    expect("missing mandatory field").eq(globalMessage);
  },

  addPetInvalid(field) {
    cy.fixture(fixture).then((data) => {
      switch (field) {
        case 'petId':
          data.id = "test";
          break;
        case 'categoryId':
          data.category.id = "test";
          break;
        case 'tagsId':
          data.tags.id = "test";
          break;
        case 'Name':
          data.name = 0;
          break;
        case 'photoUrl':
          data.data.photoUrls[0] = 0;
          break;
        case 'categoryName':
          data.category.name = 0;
          break;
        case 'tagName':
          data.tags.name = 0;
          break;
        case 'status':
          data.status = 0;
          break;
      }
    });
    let body = data.petMandatory;
    cy.request({
      method: 'POST',
      url: "https://petstore.swagger.io/v2/pet",
      body: body,
      timeout: 60000
    }).then((resp) => {
      expect(resp.status).to.eq(405);
      globalMessage = resp.body.property('errorMessage');
      gloabalMultibleUrl = false;
    });
  },

  validateInvalidType(type) {
    if (type.eq("string"))
      expect(globalMessage).contains("must be an integer");
    else
      expect(globalMessage).contains("must be a string")
  }

}
